from time import strftime
from pyfiglet import Figlet

def date(format_str='%Y %d %b, %A', font='graceful'):
    return Figlet(font=font).renderText(strftime(format_str))