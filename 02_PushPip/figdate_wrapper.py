import venv
from tempfile import TemporaryDirectory
import subprocess
import sys

with TemporaryDirectory() as dir_name:
    venv.create(dir_name, with_pip=True)
    subprocess.run(dir_name + '/Scripts/pip install pyfiglet')
    subprocess.run(dir_name + '/Scripts/python -m figdate' + ' ' + ' '.join(sys.argv[1:]))