from typing import Dict
import sys
from importlib import import_module

import inspect
from textwrap import dedent
import ast
from difflib import SequenceMatcher
from itertools import combinations

def parse(obj, prefix) -> Dict[str, str]:
    parses = {}
    for name, v in inspect.getmembers(obj):
        if inspect.isfunction(v):
            a = ast.parse(dedent(inspect.getsource(v)))
            for node in ast.walk(a):
                for attr in ['name', 'id', 'arg', 'attr']:
                    if hasattr(node, attr):
                        setattr(node, attr, 'F')
            parses[prefix + '.' + name] = ast.unparse(a)
        elif inspect.isclass(v):
            if not name.startswith('__'):
                parses |= parse(v, prefix + '.' + name)
    return parses

class A:
    def __init__(self):
        self.k = 0
    def dance(self, p: int):
        print(self.k + p)


parses = dict()
for module_name in sys.argv[1:]:
    mod = import_module(module_name)
    parses |= parse(mod, module_name)
parses = sorted(list(parses.items()))

for i, (name1, text1) in enumerate(parses):
    max, max_name = 0, None
    for name2, text2 in parses[i + 1:]:
        t = SequenceMatcher(None, text1, text2).ratio()
        if t >= max:
            max = t
            max_name = name2
    if max > 0.95:
        print(name1, max_name)
