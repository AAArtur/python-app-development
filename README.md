# Python App Development

This is a package **figdate**! This package provides a function `date(format: str, font: str) -> str`.

Example:
```
from figdate import date

print(date("%Y %d %b, %A", 'big'))
```
Result:
```
 ___   ___ ___  ___    ___  __   ______   _
|__ \ / _ \__ \|__ \  |__ \/_ | |  ____| | |
   ) | | | | ) |  ) |    ) || | | |__ ___| |__
  / /| | | |/ /  / /    / / | | |  __/ _ \ '_ \
 / /_| |_| / /_ / /_   / /_ | | | | |  __/ |_) |
|____|\___/____|____| |____||_| |_|  \___|_.__( )
                                              |/

 __  __                 _
|  \/  |               | |
| \  / | ___  _ __   __| | __ _ _   _
| |\/| |/ _ \| '_ \ / _` |/ _` | | | |
| |  | | (_) | | | | (_| | (_| | |_| |
|_|  |_|\___/|_| |_|\__,_|\__,_|\__, |
                                 __/ |
                                |___/

```
It uses **pyfiglet** inside.
This package can also be called directly like `python -m figdate <format> <font>`. Examples:

`python -m figdate` | `python -m figdate %A` | `python -m figdate %A banner`.

It uses russian locale in this case.

`figdate_wrapper.py` can help to use this package without having a **pyfiglet** package
(using virtual environment).