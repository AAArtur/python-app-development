from urllib.request import urlopen

import sys
import locale
from typing import List

from .bullscows import bullscows, gameplay

def download_vocabulary(path: str, length: int) -> List[str]:
    if path.startswith('https://') or path.startswith('http://'):
        with urlopen(path) as file:
            words = file.read().decode('utf-8').split()
    else:
        with open(path, encoding='utf-8') as file:
            words = '\n'.join(file.readlines()).split()
    
    words = [word for word in words if len(word) == length]
    return words


def congratulate(attempts: int) -> None:
    if attempts > 1:
        print('Победа! Ты угадал слово за {} попыток!'.format(attempts))
    elif attempts == 1:
        print('Не может быть! Ты угадал слово с первой попытки!')
    else:
        print('Что-то пошло не так!')

def default_ask(prompt: str, valid: List[str] = None) -> str:
    while True:
        s = input(prompt)
        if valid is None or s in valid:
            return s

def default_inform(format_string: str, bulls: int, cows: int) -> None:
    print(format_string.format(bulls, cows))


locale.setlocale(locale.LC_ALL, ('ru_RU', 'UTF-8'))

path = sys.argv[1]
length = 5 if len(sys.argv) <= 2 else int(sys.argv[2])

words = download_vocabulary(path, length)
print('Игра начинается! Длина слов: {}'.format(length))
attempts = gameplay(default_ask, default_inform, words)
congratulate(attempts)