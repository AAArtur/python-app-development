from textdistance import hamming, bag

from random import seed, choice
from typing import List

def bullscows(guess: str, secret: str) -> (int, int):
    bulls = hamming.similarity(guess, secret)
    cows = bag.similarity(guess, secret) - bulls
    return bulls, cows

def gameplay(ask: callable, inform: callable, words: List[str]) -> int:
    if len(words) == 0:
        print('Empty word list!')
        return -1
    seed()
    secret = choice(words)
    attempts = 0
    while True:
        attempts += 1
        guess = ask("Введите слово: ", words)
        bulls, cows = bullscows(guess, secret)
        inform("Быки: {}, Коровы: {}", bulls, cows)
        if bulls == len(secret):
            return attempts
