import cmd
import shlex
import pyreadline3

import pynames

class NamesRepl(cmd.Cmd):
    short = pynames.generators
    gens = {
        'scandinavian': {'traditional': short.scandinavian.ScandinavianNamesGenerator()},
        'russian': {'pagan': short.russian.PaganNamesGenerator()},
        'mongolian': {'traditional': short.mongolian.MongolianNamesGenerator()},
        'korean': {'traditional': short.korean.KoreanNamesGenerator()},
        'goblin': {'custom': short.goblin.GoblinGenerator()},
        'orc': {'custom': short.orc.OrcNamesGenerator()},
        'elven': {
            'dnd': short.elven.DnDNamesGenerator(),
            'warhammer': short.elven.WarhammerNamesGenerator(),
        },
        'iron_kingdoms': {
            'caspian_midlunder_sulese': short.iron_kingdoms.CaspianMidlunderSuleseFullnameGenerator(),
            'dwarf': short.iron_kingdoms.DwarfFullnameGenerator(),
            'gobber': short.iron_kingdoms.GobberFullnameGenerator(),
            'iossan_nyss': short.iron_kingdoms.IossanNyssFullnameGenerator(),
            'khadoran': short.iron_kingdoms.KhadoranFullnameGenerator(),
            'ogrun': short.iron_kingdoms.OgrunFullnameGenerator(),
            'ryn': short.iron_kingdoms.RynFullnameGenerator(),
            'thurian_morridane': short.iron_kingdoms.ThurianMorridaneFullnameGenerator(),
            'tordoran': short.iron_kingdoms.TordoranFullnameGenerator(),
            'trollkin': short.iron_kingdoms.TrollkinFullnameGenerator(),
        },
    }
    genders = {
        'male': 'm',
        'female': 'f',
    }
    default_gender = genders['male']
    languages = {'ru', 'en'}
    prompt = "<?> "
    
    def __init__(self):
        super().__init__()
        self.language = 'native'
        
        
    def do_help(self, args):
        help_str = """
Commands: generate, language, info, help, exit.
\tgenerate <class> <subclass> [{male | female}]
\t\tgenerate iron_kingdoms Gobber female
\tlanguage {en | ru}
\t\tlanguage ru
\tinfo <class> <subclass> [{male | female | language}]
\t\tinfo scandinavian male
\t\tinfo scandinavian
"""
        print(help_str)
    
    def do_exit(self, args):
        return True

    def do_quit(self, args):
        return True


    def class_parse(self, args):
        # "russian" -> "russian", "pagan", None
        # "russian female" -> "russian", "pagan", "female"
        # "iron_kingdoms ryn" -> "iron_kingdoms", "ryn", None
        # "iron_kingdoms ryn language" -> "iron_kingdoms", "ryn", "language"
        option = None
        
        class_name, *params = shlex.split(args)
        if len(self.gens[class_name]) == 1:
            subclass = list(self.gens[class_name].keys())[0]
            if len(params) > 0:
                option = params[0]
        else:
            subclass = params[0]
            if len(params) > 1:
                option = params[1]
        return class_name, subclass, option
    
    def do_generate(self, args):
        try:
            class_name, subclass, option = self.class_parse(args)
            if option is None:
                gender = self.default_gender
            else:
                gender = self.genders[option]
        except (KeyError, ValueError, IndexError):
            print('generate: incorrect parameters')
            return
        
        gen = self.gens[class_name][subclass]
        if self.language in gen.languages:
            lang = self.language
        else:
            lang = gen.native_language
        res = gen.get_name_simple(gender, lang)
        print(res)

    def complete_generate(self, prefix, allcmd, beg, end):
        _, *params = shlex.split(allcmd)
        current = len(params)
        if prefix == '':
            current += 1
        
        if current == 1:
            variants = self.gens.keys()
        else:
            name = params[0]
            if len(self.gens.get(name, {})) > 1:
                if current == 2:
                    variants = self.gens.get(name, {}).keys()
                elif current == 3:
                    variants = self.genders
            else:
                if current == 2:
                    variants = self.genders
                else:
                    variants = []
        return [cmd for cmd in variants if cmd.startswith(prefix)]

    def do_language(self, args):
        try:
            lang, *_ = shlex.split(args)
            if lang in self.languages:
                self.language = lang
            else:
                raise KeyError
            
        except (KeyError, ValueError, IndexError):
            print('language: incorrect parameters')

    def complete_language(self, prefix, allcmd, beg, end):
        variants = self.languages
        return [cmd for cmd in variants if cmd.startswith(prefix)]

    def do_info(self, args):
        try:
            class_name, subclass, option = self.class_parse(args)
        except (KeyError, ValueError, IndexError):
            print('info: incorrect parameters')
            return
        
        gen = self.gens[class_name][subclass]

        if option in self.genders:
            print(gen.get_names_number(self.genders[option]))
        elif option == 'language':
            print(*gen.languages)
        elif option is None:
            print(gen.get_names_number())
        else:
            print('info: incorrect parameters')
        
    def complete_info(self, prefix, allcmd, beg, end):
        _, *params = shlex.split(allcmd)
        current = len(params)
        if prefix == '':
            current += 1
        
        if current == 1:
            variants = self.gens.keys()
        else:
            name = params[0]
            if len(self.gens.get(name, {})) > 1:
                if current == 2:
                    variants = self.gens.get(name, {}).keys()
                elif current == 3:
                    variants = list(self.genders.keys()) + ['language']
            else:
                if current == 2:
                    variants = list(self.genders.keys()) + ['language']
                else:
                    variants = []
        return [cmd for cmd in variants if cmd.startswith(prefix)]

    
NamesRepl().cmdloop()
